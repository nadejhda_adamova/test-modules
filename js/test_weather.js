(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.nodeDetailsSummaries = {
    attach: function attach(context) {

      function updWeather() {
        var url = '/test-weather/get-weather';
        $.ajax({
          url: url,
          dataType: 'json',
        })
        .done(function(response) {
          $('.test-weather--wrapper').html(response);
        })
      }

      setInterval(function(){
        updWeather()
      }, (1 * 30 * 1000));


    }
  };
})(jQuery, Drupal, drupalSettings);
