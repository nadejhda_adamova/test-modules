<?php

namespace Drupal\test_weather\Service;

use GuzzleHttp\ClientInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\ConnectException;

/**
 * Class testWeatherService
 * @package Drupal\test_weather\Controller
 */
class testWeatherService
{
  /**
   * Base uri of openweather api.
   *
   * @var Drupal\test_weather
   */
  public static $baseUri = 'http://api.openweathermap.org/';

  /**
   * The HTTP client to fetch the feed data with.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * Constructs a database object.
   *
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The Guzzle HTTP client.
   */
  public function __construct(ClientInterface $http_client) {
    $this->httpClient = $http_client;
  }

  /**
   * Get a complete query for the API.
   */
  public function createRequest($options) {
    $query = [];
    $query['appid'] = $options['test_weather_api_key'];
    // $city_string = $options['test_weather_city_id'];
    // $sub = substr($city_string, strpos($city_string, '(')+strlen('('), strlen($city_string));
    // $coords = substr($sub, 0, strpos($sub, ')'));
    // $coords = explode(',', $coords);
    // $query['lat'] = $coords[1];
    // $query['lon'] = $coords[0];
    $query['id'] = $options['test_weather_city_id'];
    return $query;
  }

  /**
   * @return JsonResponse
   */
  public function currentWeatherData($options) {
    try {
      $response = $this->httpClient->request('GET', self::$baseUri . 'data/2.5/weather',
      [
        'query' => $this->createRequest($options),
      ]);
    }
    catch (GuzzleException $e) {
      watchdog_exception('test_weather', $e);
      return FALSE;
    }
    return $response->getBody()->getContents();
  }

  /**
   * Return an array containing the current weather information.
   */
  public function getCurrentWeatherInformation($output, $config) {
    $data = [];
    foreach ($output as $key => $item) {
      switch ($key) {
        case 'weather':
          $data['icon'] = self::$baseUri . '/img/w/' . $item[0]['icon'] . '.png';
          $data['desc'] = $item[0]['description'];
          break;
        case 'main':
          $data['humidity'] = $item['humidity'] . '%';
          $data['temp'] = round($item['temp'] - 273.15) . '°C';
          break;
        case 'wind':
          $data['wind_speed'] = round($item['speed'] * (60 * 60 / 1000), 1) . ' km/h';
          $data['wind_deg'] = $item['deg'];
          break;
      }
    }

    $build[] = [
      '#theme' => 'test_weather_block',
      '#data' => $data,
      '#attached' => [
        'library' => [
          'test_weather/test_weather_theme',
        ],
      ],
      '#cache' => ['max-age' => 0],
    ];
    return $build;
  }

}
