<?php

namespace Drupal\test_weather\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\test_weather\Service\testWeatherService;

/**
 * Provides a 'Weather' Block.
 *
 * @Block(
 *   id = "weather_block",
 *   admin_label = @Translation("Weather block"),
 *   category = @Translation("Weather information"),
 * )
 */
class WeatherBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The module handler.
   *
   * @var \Drupal\test_weather\Service\testWeatherGetWeather
   */
  protected $weatherservice;

  /**
   * Constructs a Drupal\Component\Plugin\PluginBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @var string $weatherservice
   *   The information from the Weather service for this block.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, testWeatherService $weatherservice) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->weatherservice = $weatherservice;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('test_weather.weather_service')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $config = $this->getConfiguration();
    $form['city_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('City'),
      '#default_value' => !empty($config['test_weather_city_id']) ? $config['test_weather_city_id'] : '',
      '#description' => t('Please, start enter city name'),
      '#autocomplete_route_name' => 'test_weather.autocomplete',
    ];
    $form['test_weather_api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API key from Open Weather'),
      '#default_value' => !empty($config['test_weather_api_key']) ? $config['test_weather_api_key'] : '',
      '#description' => t('Please, eneter your API key from your account on <a href="https://openweathermap.org">OpenWeatherMap</a>'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->setConfigurationValue('test_weather_city_id', $form_state->getValue('city_id'));
    $this->setConfigurationValue('test_weather_api_key', $form_state->getValue('test_weather_api_key'));
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->getConfiguration();

    $output = json_decode($this->weatherservice->currentWeatherData($config), TRUE);
    if (!empty($output)) {
      $build = $this->weatherservice->getCurrentWeatherInformation($output, $config);
      return $build;
    }
  }

}
