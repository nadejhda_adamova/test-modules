<?php

namespace Drupal\test_weather\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Component\Utility\Xss;

/**
 * Class JsonApiCitiesController
 * @package Drupal\test_weather\Controller
 */
class JsonApiCitiesController
{

  /**
   * @return JsonResponse
   */
  public function handleAutocomplete(Request $request)
  {
    $results = [];
    $input = $request->query->get('q');
    if (!$input) {
      return new JsonResponse($results);
    }

    $input = Xss::filter($input);

    if(strlen(trim($input)) > 3){
      $module_handler = \Drupal::service('module_handler');
      $module_path = $module_handler->getModule('test_weather')->getPath();

      $string = file_get_contents($module_path . "/data/city.list.json");
      $json_a = json_decode($string, true);

      foreach($json_a as $item) {
        if (stripos($item['name'], $input) !== false) {
          $results[] = [
            'value' => $item['id'],
            'label' => $item['name'] . ' (' . $item['country'] . ', id "' . $item['id'] . '", lon "' . $item['coord']['lon']. '", lat "' . $item['coord']['lat'] . '")',
          ];
        }
      }
    }
    return new JsonResponse($results);
  }
}
