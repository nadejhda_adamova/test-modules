<?php

namespace Drupal\test_weather\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\test_weather\Service\testWeatherService;

/**
 * Controller for export json.
 */
class ExportWeatherController extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  public function data() {
    $block_id = 'weatherblock';
    $markup = '';
    $block = \Drupal\block\Entity\Block::load($block_id);
    if ($block) {
      $settings = $block->get('settings');
      $data = \Drupal::service('test_weather.weather_service')->currentWeatherData($settings);
      $output = json_decode(\Drupal::service('test_weather.weather_service')->currentWeatherData($settings), TRUE);
      if (!empty($output)) {
        $build = \Drupal::service('test_weather.weather_service')->getCurrentWeatherInformation($output, $config);
        $markup = render($build);
      }
    }
    return new JsonResponse($markup);
  }
}
